### Présentation du projet
Ce projet à pour but de réaliser un mini-jeu Pokemon en lignes de commandes en Python. Il s'agit du projet informatique de deuxième année de l'ENSAI.
Nous sommes 5 à travailler sur le projet: Victoire Aussudre, Hela Dellagi, Marine Emorine, Dorian Grousset et Antoine Prévot, sous la direction de Sylvain Leveugle et de Rémi Pépin. Nous utilisons pokeapi.co pour récupérer des données sur les pokemon.

### Fonctionnalités 
- Un joueur peut se connecter ou créer un compte. 

- Un joueur commence avec un certains nombre de pokeball mais sans pokemon. Il doit donc commencer par capturer un pokemon parmis les 151 de la première génération. 

- La carte est décomposés en 11 zones. Chaque zone contient aleatoirement certains pokemon de la première génération. 

- Un joueur peu se déplacer de zones en zones mais pas au sein des zones.

- Un joueur peu afficher les pokemon présents dans une zone et choisir d'en combattre un ou d'en capturer un. 

- Deux pokecenter sont présents sur la carte. 

- Dans un pokecenter un joueur peu acheter des baies ou des pokeball en fonction de son argent restant.

- Un joueur possède donc de l'argent qu'il gagne en combattant des dresseurs.

- Un joueur peu donc combattre des dresseurs. La victoire dans ces combats conditionne le passage d'une zone à une autre.

### Utillisation du jeu
Il est nécessaire d'avoir Python 3.X installé sur son ordinateur.

L'installation des packages suivant est nécessaire: tqdm, requests, pyfiglet, prettytable, PyInquirer. Dans un terminal:

```bash
pip install tqdm
pip install requests
pip install pyfiglet
pip install prettytable
pip install PyInquirer
```
Pour lancer l'application, il suffit d'écrire dans un terminal et à la racine du projet:

```bash
python main.py
```

Des faux comptes utilisateurs sont créés par défaut. Ainsi, vous pouvez vous connecter avec le nom d'utilsateur
"michael" et le mot de passe "fauxCompte".

**Attention : Si vous souhaitez créer votre propre compte il est nécessaire d'avoir un pseudo en minuscule !**

### Base de données

L'application stock au besoin des informations dans une base de données sqlite. Aucun module n'est a télécharger
puisque sqlite est dans la bibliothèque par défaut de Python. La base de données sera automatiquement créée à la 
première exécution du programme. La base se nomme data.sql. 

**Attention : Le premier chargement des données est très long et sera nécessaire si vous supprimer le fichier 
data.sql.**

### Troubleshooting

Si vous rencontrez des bugs qui vous semblent bizarres et/ou non résolubles facilement, une façon radicale de refaire 
fonctionner l'application est de supprimer le fichier data.sql qui sera recréé automatiquement lors de la prochaine
exécution. 

### **ENJOY** ! 

