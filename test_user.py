import unittest
from business_objects.pokemon import *
from business_objects.user import *


class UtilisateurTest(unittest.TestCase):
    """
        Test case utilisé pour tester les fonctions du module 'user'
        """

    def setUp(self):
        dbSession = dao()
        pok1SQL = dbSession.filterBy.get(tablename="pokemon", name="pikachu")[0]
        self.pikachu = pokemon(*pok1SQL)

        dbSession = dao()
        pok2SQL = dbSession.filterBy.get(tablename="pokemon", name="charmeleon")[0]
        self.charmeleon = pokemon(*pok2SQL)

        dbSession = dao()
        userSQL = dbSession.filterBy.get(tablename="user", username="jessica")[0]
        self.testeur = Utilisateur(*userSQL)


    def test_captureOk(self):
        """Test pour vérifier que la capture fonctionne et apparaît bien dans la liste des Pokemons capturés"""
        self.testeur.pokeball[0] = 5
        for i in range(5):
            self.testeur.capturer(pokeObj=self.pikachu)
        self.assertIn(self.pikachu.id[0], ast.literal_eval(self.testeur.pokCollection[0]))

    def test_setPkm(self):
        pass

    """
    def test_NoCapture(self):
        #Test pour vérifier que la capture fonctionne et apparaît bien dans la liste des Pokemons capturés
        self.testeur.pokeball[0]=0
        self.testeur.capturer(pokeObj=self.charmeleon)
        self.assertNotIn(self.charmeleon.id[0], ast.literal_eval(self.testeur.pokCollection[0]))"""

    def test_retirer(self):
        """

        :return:
        """
        nvPoke = pokemon(uuid_="abc123", state="Wild", name="Tintin", typeArrayStr=json.dumps(['rock']),
                         movesArrStr=json.dumps(["move1", "move2"]), xp=1300, zone=9, hp=100)
        dbSession = dao(nvPoke)
        dbSession.add()

        dbSession = dao()
        tintinSQL = dbSession.filterBy.get(tablename="pokemon", id="abc123")[0]
        tintin = pokemon(*tintinSQL)

        self.testeur.retirer(tintin)
        dbSession = dao()

        self.assertNotIn(tintin.id[0], dbSession.filterBy.get(tablename="pokemon", id=tintin.id[0]))



    def test_NoShop(self):
        """Test pour vérifier que les achats sont bien déduits des biens du joueur
        lorsque ce montant est inférieur à son argent"""
        self.testeur.coin[0] = 100
        self.testeur.pokeball[0] = 3
        self.testeur.berry[0] = 3
        self.testeur.shop(num_pokebal=1, num_berry=1)
        self.assertEqual([100 - (10 + 10), 3 + 1, 3 + 1],
                         [self.testeur.coin[0], self.testeur.pokeball[0], self.testeur.berry[0]])

    def test_NoShop(self):
        """Test pour vérifier que les achats n'ont pas lieu lorsque
        le montant est strictement supérieur à l'argent du joeuur """
        self.testeur.coin[0] = 20
        self.testeur.pokeball[0] = 2
        self.testeur.berry[0] = 2
        self.testeur.shop(num_pokebal=12, num_berry=4)
        self.assertEqual([20, 2, 2],
                         [self.testeur.coin[0], self.testeur.pokeball[0], self.testeur.berry[0]])

    def test_move(self):
        zone_aleat = random.choice(list(range(1, 11)))
        self.assertTrue(self.testeur.move(targetZone=zone_aleat))


    def test_heal(self):
        """Test pour vérifier que l'utilisation des baies permettent de soigner un Pokemon"""
        self.testeur.berry[0] = 2
        self.testeur.heal(self.pikachu)
        self.assertEqual([1, 100], [self.testeur.berry[0], self.pikachu.hp[0]])

    def test_Noheal(self):
        """Test pour vérifier que l'utilisation des baies permettent de soigner un Pokemon"""
        self.testeur.berry[0] = 0
        self.testeur.heal(self.pikachu)
        self.assertEqual([0, 100], [self.testeur.berry[0], self.pikachu.hp[0]])


if __name__ == '__main__':
    unittest.main()
